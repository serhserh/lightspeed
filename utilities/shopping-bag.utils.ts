import type { UiState } from "~/stores/ui";

export const SHOPPING_CARD_KEY = 'APP_shoppingBagItems'
export const getShoppingBagItems = (): number[] => {
    const savedShoppingBag = useCookie<number[] | undefined>(SHOPPING_CARD_KEY);
    if (!savedShoppingBag.value) return [];
    const parsed = savedShoppingBag.value;
    if (parsed satisfies UiState['shoppingBag']['items']) return parsed;
    return [];
}

export const saveShoppingBagItems = (ids: number[]) => {
    const cookie = useCookie(SHOPPING_CARD_KEY, {
        maxAge:
            60 * 60 * 24 * 7,
        path: '/',
        secure: true,
        sameSite: 'strict'
    })
    cookie.value = JSON.stringify(ids)
}