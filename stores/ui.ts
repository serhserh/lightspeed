import type { Product } from "~/models/products.models";
import { getShoppingBagItems, saveShoppingBagItems } from "~/utilities/shopping-bag.utils";

export interface UiState {
    shoppingBag: ShoppingBag
}

interface ShoppingBag {
    initialized: boolean;
    items: number[];
}

const initialState: UiState = {
    shoppingBag: {
        initialized: false,
        items: [],
    }
}

export const useUiStore = defineStore('ui', {
    state: (): UiState => ({ ...initialState }),
    getters: {
        getShoppingBag: (state) => state.shoppingBag,
    },
    actions: {
        initShoppingBag() {
            this.shoppingBag = {
                initialized: true,
                items: getShoppingBagItems()
            }
        },
        addToShoppingBag(product: Product) {
            this.shoppingBag.items.push(product.id)
            saveShoppingBagItems(this.shoppingBag.items)
        },
        removeFromShoppingBag(id: number) {
            this.shoppingBag.items = this.shoppingBag.items.filter((item) => item !== id)
            saveShoppingBagItems(this.shoppingBag.items)
        },
        clearShoppingBag() {
            this.shoppingBag.items = []
            saveShoppingBagItems(this.shoppingBag.items)
        },
    },
})