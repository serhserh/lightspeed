import type { ApiResponse } from "~/models/api.models"
import type { Category } from "~/models/categories.models"

interface CategoriesState {
  isLoading: boolean
  response?: ApiResponse<Category[]>
}

const initialState: CategoriesState = {
  isLoading: false,
}

export const useCategoriesStore = defineStore('categories', {
  state: (): CategoriesState => ({ ...initialState }),
  getters: {
    getCategories: (state) => [...state.response?.items || []],
  },
  actions: {
    async loadCategories() {
      if (this.response) return
      this.isLoading = true

      try {
        const response = await useFetch<ApiResponse<Category[]>>('/api/categories')

        if (response.error.value) {
          console.error(response.error.value)
        }

        if (response.data.value) {
          this.response = response.data.value;
        }
      } finally { this.isLoading = false }
    },
  },
})