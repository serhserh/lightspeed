/** @type {import('tailwindcss').Config} */
export default {
 content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./app.vue",
    // "./plugins/**/*.{js,ts}",
    // "./error.vue",
  ],
  theme: {
    screens: {
      xs: "375px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      '2xl': "1536px",
    },
    animation: {
      reveal: "reveal 0.5s ease-in forwards",
      spin: "spin 1s linear infinite",
    },
    keyframes: {
      reveal: {
        "0%": {
          opacity: 0,
        },
        "100%": {
          opacity: 1
        },
      },
      spin: {
        from: {
          transform: "rotate(0deg)",
        },
        to: {
          transform: "rotate(360deg)",
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

