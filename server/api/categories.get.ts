import axios from 'axios'
import type { EventHandlerRequest, H3Event } from 'h3'
import { handleApiError } from '~/models/api.models'

export default defineEventHandler(async (event: H3Event<EventHandlerRequest>) => {
    const { category } = getQuery(event)
    try {
        const response = await axios(`${process.env.ECWID_BASE_URL}/${process.env.ECWID_STORE_ID}/categories${category ? `/${category}` : ''}`, {
            headers: { "Authorization": `Bearer ${process.env.ECWID_BEARER_TOKEN}` }
        })
        return response.data;
    } catch (error) {
        return handleApiError(error)
    }
})