import axios from 'axios'
import type { EventHandlerRequest, H3Event } from 'h3'
import { handleApiError } from '~/models/api.models'

export default defineEventHandler(async (event: H3Event<EventHandlerRequest>) => {
    const { offset, category, id, products } = getQuery(event)
    try {
        const response = await axios(`${process.env.ECWID_BASE_URL}/${process.env.ECWID_STORE_ID}/products${id ? `/${id}` : ''}?limit=8` +
            `${offset ? `&offset=${offset}` : ''}` +
            `${category ? `&category=${category}` : ''}` +
            `${products ? `&productId=${products}` : ''}
            `,
            {
                headers: { "Authorization": `Bearer ${process.env.ECWID_BEARER_TOKEN}` }
            })
        return response.data;
    } catch (error) {
        return handleApiError(error)
    }
})