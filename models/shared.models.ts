export interface Dictionary<T> {
    [key: string]: T;
}

export interface Image {
    url: string;
    width: number;
    height: number;
}

export interface BorderInfo {
    dominatingColor: {
        red: number;
        green: number;
        blue: number;
        alpha: number;
    };
    homogeneity: boolean;
}