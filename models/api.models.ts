import { AxiosError } from "axios";

export class ApiError extends Error {
    statusCode: number;

    constructor(message: string, statusCode?: number) {
        super(message);
        this.statusCode = statusCode || 500;

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, this.constructor);
        }

        Object.setPrototypeOf(this, ApiError.prototype);
    }
}

export interface ApiResponse<T> {
    items: T
    offset: number
    total: number
    count: number
    limit: number
}

export const handleApiError = (error: any) => {
    console.log(error)
    if (error instanceof AxiosError) {
        return error;
    }
    return new ApiError('Failed to fetch categories', 500)
}